<?php
/**
 * AvaTaxServiceSoap.class.php
 */

/**
 * Proxy interface for the Avalara Tax Web Service.  It contains methods that perform remote calls
 * to the Avalara Tax Service.
 *
 * AvaTaxServiceSoap reads its configuration values from static variables defined
 * in AvaATConfig.class.php. This file must be properly configured with your security credentials.
 *
 * <p>
 * <b>Example:</b>
 * <pre>
 *  $taxService = new AvaTaxServiceSoap();
 *  $result = $taxService->ping();
 * </pre>
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */


class AvaTaxServiceSoap extends AvaAvalaraSoapClient
{
    static $servicePath = '/Tax/TaxSvc.asmx';
    static protected $classmap = array(
        'BaseAddress' => '\Avalara\AddressSvc\AvaAddress',
        'ValidAddress' => '\Avalara\AddressSvc\AvaValidAddress',
        'Message' => '\Avalara\BaseSvc\AvaMessage',
        'ValidateRequest' => '\Avalara\AddressSvc\AvaValidateRequest',
        'IsAuthorizedResult' => '\Avalara\BaseSvc\AvaIsAuthorizedResult',
        'PingResult' => '\Avalara\BaseSvc\AvaPingResult',
        'ValidateResult' => '\Avalara\AddressSvc\AvaValidateResult',
		'Line'=>'\Avalara\TaxSvc\AvaLine',
		'AdjustTaxRequest'=>'\Avalara\TaxSvc\AvaAdjustTaxReqGetTaxHistoryResultuest',
		'AdjustTaxResult'=>'\Avalara\TaxSvc\AvaAdjustTaxResult',
		'CancelTaxRequest'=>'\Avalara\TaxSvc\AvaCancelTaxRequest',
		'CancelTaxResult'=>'\Avalara\TaxSvc\AvaCancelTaxResult',
		'CommitTaxRequest'=>'\Avalara\TaxSvc\AvaCommitTaxRequest',
		'CommitTaxResult'=>'\Avalara\TaxSvc\AvaCommitTaxResult',
		'GetTaxRequest'=>'\Avalara\TaxSvc\AvaGetTaxRequest',
		'GetTaxResult'=>'\Avalara\TaxSvc\AvaGetTaxResult',
		'GetTaxHistoryRequest'=>'\Avalara\TaxSvc\AvaGetTaxHistoryRequest',
		'GetTaxHistoryResult'=>'\Avalara\TaxSvc\AvaGetTaxHistoryResult',
		'PostTaxRequest'=>'\Avalara\TaxSvc\AvaPostTaxRequest',
		'PostTaxResult'=>'\Avalara\TaxSvc\AvaPostTaxResult',
		'ReconcileTaxHistoryRequest'=>'\Avalara\TaxSvc\AvaReconcileTaxHistoryRequest',
		'ReconcileTaxHistoryResult'=>'\Avalara\TaxSvc\AvaReconcileTaxHistoryResult',
		'TaxLine'=>'\Avalara\TaxSvc\AvaTaxLine',
        'TaxDetail' => '\Avalara\TaxSvc\AvaTaxDetail',
		'ApplyPaymentRequest'=>'\Avalara\TaxSvc\AvaApplyPaymentRequest',
		'ApplyPaymentResult'=>'\Avalara\TaxSvc\AvaApplyPaymentResult',
		'BaseResult'=>'\Avalara\Base\AvaBaseResult',
		'TaxOverride'=>'\Avalara\TaxSvc\AvaTaxOverride'
		);

public function __construct($configurationName = 'Default')
    {
        $config = new AvaATConfig($configurationName);

        $this->client = new AvaDynamicSoapClient   (
            $config->taxWSDL,
            array
            (
                'location' => $config->url.$config->taxService,
                'trace' => $config->trace,
                'classmap' => AvaTaxServiceSoap::$classmap
            ),
            $config
        );
    }



    /**
     * Calculates taxes on a document such as a sales order, sales invoice, purchase order, purchase invoice, or credit memo.
     * <br>The tax data is saved Sales Invoice and Purchase Invoice document types {@link AvaGetTaxRequest#getDocType}.
     *
     * @param getTaxRequest  -- Tax calculation request
     *
     * @return GetTaxResult
     * @throws SoapFault
     */
    public function getTax(&$getTaxRequest)
    {
		$getTaxRequest->prepare();
		return $this->client->GetTax(array('GetTaxRequest' => $getTaxRequest))->GetTaxResult;
    }

    /**
     * Retrieves a previously calculated tax document.
     * <p>
     * This is only available for saved tax documents (Sales Invoices, Purchase Invoices).
     * </p>
     * <p>
     * A document can be indicated solely by the {@link AvaPostTaxRequest#getDocId} if it is known.
     * Otherwise the request must specify all of {@link AvaPostTaxRequest#getCompanyCode}, see {@link AvaPostTaxRequest#getDocCode}
     * and {@link AvaPostTaxRequest#getDocType} in order to uniquely identify the document.
     * </p>
     *
     * @param getTaxHistoryRequest a {@link AvaGetTaxHistoryRequest} object indicating the document for which history should be retrieved.
     * @return a {@link AvaGetTaxHistoryResult} object
     * @throws SoapFault
     */
    /*public com.avalara.avatax.services.tax.AvaGetTaxHistoryResult getTaxHistory(com.avalara.avatax.services.tax.AvaGetTaxHistoryRequest getTaxHistoryRequest) throws SoapFault;
	*/
	public function getTaxHistory(&$getTaxHistoryRequest)
    {
		$result = $this->client->GetTaxHistory(array('GetTaxHistoryRequest'=>$getTaxHistoryRequest))->GetTaxHistoryResult;
		$result->getGetTaxRequest()->postFetch();
		return $result;
    }

    /**
     * Posts a previously calculated tax
     * <p>
     * This is only available for saved tax documents (Sales Invoices, Purchase Invoices).
     * </p>
     * <p>
     * A document can be indicated solely by the {@link AvaPostTaxRequest#getDocId} if it is known.
     * Otherwise the request must specify all of {@link AvaPostTaxRequest#getCompanyCode}, {@link AvaPostTaxRequest#getDocCode}, and
     * {@link AvaPostTaxRequest#getDocType} in order to uniquely identify the document.
     * </p>
     *
     * @param postTaxRequest a {@link AvaPostTaxRequest} object indicating the document that should be posted.
     * @return a {@link AvaPostTaxResult} object
     * @throws SoapFault
     */

    /*public com.avalara.avatax.services.tax.AvaPostTaxResult postTax(com.avalara.avatax.services.tax.AvaPostTaxRequest postTaxRequest) throws SoapFault;
	*/
    public function postTax(&$postTaxRequest)
    {
		return $this->client->PostTax(array('PostTaxRequest'=>$postTaxRequest))->PostTaxResult;
    }

    /**
     * Commits a previously posted tax.
     * <p>
     * This is only available for posted tax documents (Sales Invoices, Purchase Invoices). Committed documents cannot
     * be changed or deleted.
     * </p>
     * <p>
     * A document can be indicated solely by the {@link AvaCommitTaxRequest#getDocId} if it is known. Otherwise the
     * request must specify all of {@link AvaCommitTaxRequest#getCompanyCode}, {@link AvaCommitTaxRequest#getDocCode}, and
     * {@link AvaCommitTaxRequest#getDocType} in order to uniquely identify the document.
     * </p>
     *
     * @param commitTaxRequest a {@link AvaCommitTaxRequest} object indicating the document that should be committed.
     * @return a {@link CommitTaxResult} object
     * @throws SoapFault
     */

    /*public com.avalara.avatax.services.tax.CommitTaxResult commitTax(com.avalara.avatax.services.tax.AvaCommitTaxRequest commitTaxRequest) throws SoapFault;
	*/
	public function commitTax(&$commitTaxRequest)
    {
		return $this->client->CommitTax(array('CommitTaxRequest'=>$commitTaxRequest))->CommitTaxResult;
    }
    /**
     * Cancels a previously calculated tax;  This is for use as a
     * compensating action when posting on the client fails to complete.
     * <p>
     * This is only available for saved tax document types (Sales Invoices, Purchase Invoices). A document that is saved
     * but not posted will be deleted if canceled. A document that has been posted will revert to a saved state if canceled
     * (in this case <b>CancelTax</b> should be called with a {@link CancelTaxRequest#getCancelCode} of
     * <i>PostFailed</i>). A document that has been committed cannot be reverted to a posted state or deleted. In the case
     * that a document on the client side no longer exists, a committed document can be virtually removed by calling
     * <b>CancelTax</b> with a <b>CancelCode</b> of <i>DocDeleted</i>. The record will be retained in history but removed
     * from all reports.
     * </p>
     * <p>
     * A document can be indicated solely by the {@link CancelTaxRequest#getDocId} if it is known. Otherwise the request
     * must specify all of {@link CancelTaxRequest#getCompanyCode}, {@link CancelTaxRequest#getDocCode}, and
     * {@link CancelTaxRequest#getDocType} in order to uniquely identify the document.
     *
     * @param cancelTaxRequest a {@link CancelTaxRequest} object indicating the document that should be canceled.
     * @return   a {@link AvaCancelTaxResult} object
     * @throws SoapFault
     */
     /* public com.avalara.avatax.services.tax.AvaCancelTaxResult cancelTax(com.avalara.avatax.services.tax.CancelTaxRequest cancelTaxRequest) throws SoapFault;
	 */
	public function cancelTax(&$cancelTaxRequest)
    {
		return $this->client->CancelTax(array('CancelTaxRequest'=>$cancelTaxRequest))->CancelTaxResult;
		return $this->client->CancelTax(array('CancelTaxRequest'=>$cancelTaxRequest))->CancelTaxResult;
    }

    /**
     * Reconciles tax history to ensure the client data matches the
     * AvaTax history.
     * <p>The Reconcile operation allows reconciliation of the AvaTax history with the client accounting system.
     * It must be used periodically according to your service contract.
     * </p>
     * <p>
     * Because there may be a large number of documents to reconcile, it is designed to be called repetitively
     * until all documents have been reconciled.  It should be called until no more documents are returned.
     * Each subsequent call should pass the previous results {@link AvaReconcileTaxHistoryRequest#getLastDocId}.
     * </p>
     * <p>
     * When all results have been reconciled, Reconcile should be called once more with
     * {@link AvaReconcileTaxHistoryRequest#getLastDocId}
     * equal to the last document code processed and {@link AvaReconcileTaxHistoryRequest#isReconciled} set to true to indicate
     * that all items have been reconciled.  If desired, this may be done incrementally with each result set.  Just send
     * Reconciled as true when requesting the next result set and the prior results will be marked as reconciled.
     * </p>
     * <p>
     * The {@link #postTax}, {@link #commitTax}, and {@link #cancelTax} operations can be used to correct any differences.
     * {@link #getTax} should be called if any committed documents are out of balance
     * ({@link AvaGetTaxResult#getTotalAmount} or {@link AvaGetTaxResult#getTotalTax}
     * don't match the accounting system records).  This is to make sure the correct tax is reported.
     * </p>
     *
     * @param reconcileTaxHistoryRequest  a Reconciliation request
     * @return A collection of documents that have been posted or committed since the last reconciliation.
     * @throws SoapFault
     */
    /*public com.avalara.avatax.services.tax.AvaReconcileTaxHistoryResult reconcileTaxHistory(com.avalara.avatax.services.tax.AvaReconcileTaxHistoryRequest reconcileTaxHistoryRequest) throws SoapFault;
*/
	public function reconcileTaxHistory(&$reconcileTaxHistoryRequest)
    {
		return $this->client->ReconcileTaxHistory(array('ReconcileTaxHistoryRequest'=>$reconcileTaxHistoryRequest))->ReconcileTaxHistoryResult;
    }

/**
     * Adjusts a previously calculated tax.
     * <p>
     * This is only available for unlocked tax documents (Sales Invoices, Purchase Invoices).      * </p>
     * <p>
      * </p>
     *
     * @param adjustTaxRequest a {@link AvaAdjustTaxRequest} object indicating the document that should be edited.
     * @return a {@link AvaAdjustTaxResult} object
     * @throws SoapFault
     */

    /*public com.avalara.avatax.services.tax.CommitTaxResult commitTax(com.avalara.avatax.services.tax.AvaCommitTaxRequest commitTaxRequest) throws SoapFault;
	*/
	public function adjustTax(&$adjustTaxRequest)
    {
		$adjustTaxRequest->getGetTaxRequest()->prepare();
		return $this->client->AdjustTax(array('AdjustTaxRequest'=>$adjustTaxRequest))->AdjustTaxResult;
    }
    /**
     * Checks authentication of and authorization to one or more
     * operations on the service.
     *
     * This operation allows pre-authorization checking of any
     * or all operations. It will return a comma delimited set of
     * operation names which will be all or a subset of the requested
     * operation names.  For security, it will never return operation
     * names other than those requested (no phishing allowed).
     *
     * <b>Example:</b><br>
     * <code> isAuthorized("GetTax,PostTax")</code>
     *
     * @param string $operations  a comma-delimited list of operation names
     *
     * @return AvaIsAuthorizedResult
     * @throws SoapFault
     */


    public function isAuthorized($operations)
    {
        return $this->client->IsAuthorized(array('Operations' => $operations))->IsAuthorizedResult;
    }

    /**
     * Verifies connectivity to the web service and returns version
     * information about the service.
     *
     * <b>NOTE:</b>This replaces TestConnection and is available on
     * every service.
     *
     * @param string $message for future use
     * @return AvaPingResult
     * @throws SoapFault
     */

    public function ping($message = '')
    {
        return $this->client->Ping(array('Message' => $message))->PingResult;
    }

    /**
     * This method is used to apply a payment to a document for cash basis accounting. Applies a payment date to an existing invoice
	 * It sets the document PaymentDate and changes the reporting date from the DocDate default. It may be called before or after a document is committed. It should not be used for accrual basis accounting
     *
     * @param AvaApplyPaymentRequest $applyPaymentRequest
     * @return ApplyPaymentResult
     */

    public function applyPayment(&$applyPaymentRequest)
    {
		return $this->client->ApplyPayment(array('ApplyPaymentRequest' => $applyPaymentRequest))->ApplyPaymentResult;
    }

}