<?php
/**
 * AvaBatchSvc.class.php
 */

/**
 * 
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */
use Avalara\BatchSvc\AvaBatchFetchResult as AvaBatchFetchResult;

class AvaBatchSvc extends SoapClient {

  	private $client;
	private static $classmap = array(
                                    'BatchFetch' => '\Avalara\BatchSvc\AvaBatchFetch',
                                    'FetchRequest' => '\Avalara\BatchSvc\AvaFetchRequest',
                                    'BatchFetchResponse' => '\Avalara\BatchSvc\AvaBatchFetchResponse',
                                    'BatchFetchResult' => '\Avalara\BatchSvc\AvaBatchFetchResult',
                                    'BaseResult' => '\Avalara\BatchSvc\AvaBaseResult',
                                    'SeverityLevel' => '\Avalara\BaseSvc\AvaSeverityLevel',
                                    'Message' => '\Avalara\BaseSvc\AvaMessage',
                                    'Batch' => '\Avalara\BatchSvc\AvaBatch',
                                    'BatchFile' => '\Avalara\BatchSvc\AvaBatchFile',
                                    'Profile' => '\Avalara\BatchSvc\AvaProfile',
                                    'BatchSave' => '\Avalara\BatchSvc\AvaBatchSave',
                                    'BatchSaveResponse' => '\Avalara\BatchSvc\AvaBatchSaveResponse',
                                    'BatchSaveResult' => '\Avalara\BatchSvc\AvaBatchSaveResult',
                                    'AuditMessage' => '\Avalara\BatchSvc\AvaAuditMessage',
                                    'BatchDelete' => '\Avalara\BatchSvc\AvaBatchDelete',
                                    'DeleteRequest' => '\Avalara\BatchSvc\AvaDeleteRequest',
                                    'FilterRequest' => '\Avalara\BatchSvc\AvaFilterRequest',
                                    'BatchDeleteResponse' => '\Avalara\BatchSvc\AvaBatchDeleteResponse',
                                    'DeleteResult' => '\Avalara\BatchSvc\AvaDeleteResult',
                                    'FilterResult' => '\Avalara\BatchSvc\AvaFilterResult',
                                    'BatchProcess' => '\Avalara\BatchSvc\AvaBatchProcess',
                                    'BatchProcessRequest' => '\Avalara\BatchSvc\AvaBatchProcessRequest',
                                    'BatchProcessResponse' => '\Avalara\BatchSvc\AvaBatchProcessResponse',
                                    'BatchProcessResult' => '\Avalara\BatchSvc\AvaBatchProcessResult',
                                    'BatchFileFetch' => '\Avalara\BatchSvc\AvaBatchFileFetch',
                                    'BatchFileFetchResponse' => '\Avalara\BatchSvc\AvaBatchFileFetchResponse',
                                    'BatchFileFetchResult' => '\Avalara\BatchSvc\AvaBatchFileFetchResult',
                                    'BatchFileSave' => '\Avalara\BatchSvc\AvaBatchFileSave',
                                    'BatchFileSaveResponse' => '\Avalara\BatchSvc\AvaBatchFileSaveResponse',
                                    'BatchFileSaveResult' => '\Avalara\BatchSvc\AvaBatchFileSaveResult',
                                    'BatchFileDelete' => '\Avalara\BatchSvc\AvaBatchFileDelete',
                                    'BatchFileDeleteResponse' => '\Avalara\BatchSvc\AvaBatchFileDeleteResponse',
                                    'Ping' => '\Avalara\BatchSvc\AvaPing',
                                    'PingResponse' => '\Avalara\BatchSvc\AvaPingResponse',
                                    'PingResult' => '\Avalara\BaseSvc\AvaPingResult',
                                    'IsAuthorized' => '\Avalara\BatchSvc\AvaIsAuthorized',
                                    'IsAuthorizedResponse' => '\Avalara\BatchSvc\AvaIsAuthorizedResponse',
                                    'IsAuthorizedResult' => '\Avalara\BaseSvc\AvaIsAuthorizedResult',
                                   );

	public function __construct($configurationName = 'Default')
    {
        $config = new AvaATConfig($configurationName);
        
        $this->client = new AvaDynamicSoapClient   (
            $config->batchWSDL,
            array
            (
                'location' => $config->url.$config->batchService, 
                'trace' => $config->trace,
                'classmap' => AvaBatchSvc::$classmap
            ), 
            $config
        );
    }    

  /**
   * Fetches one or more Batch 
   *
   * @param AvaBatchFetch $parameters
   * @return AvaBatchFetchResponse
   */  
    public function BatchFetch(&$fetchRequest) {    
      
      return $this->client->BatchFetch(array('FetchRequest' => $fetchRequest))->getBatchFetchResult();
  }

  /**
   * Saves a Batch entry 
   *
   * @param AvaBatchSave $parameters
   * @return AvaBatchSaveResponse
   */
  public function BatchSave(&$batch) {
   	
  	return $this->client->BatchSave(array('Batch' => $batch))->getBatchSaveResult();
  	
  }

  /**
   * Deletes one or more Batches 
   *
   * @param AvaBatchDelete $parameters
   * @return AvaBatchDeleteResponse
   */
  public function BatchDelete(&$deleteRequest) {
     	
  	return $this->client->BatchDelete(array('DeleteRequest' => $deleteRequest))->getBatchDeleteResult();
  	
  }

  /**
   * Processes one or more Batches 
   *
   * @param AvaBatchProcess $parameters
   * @return AvaBatchProcessResponse
   */
  public function BatchProcess(&$batchProcessRequest) {
    
  	return $this->client->BatchProcess(array('BatchProcessRequest' => $batchProcessRequest))->getBatchProcessResult();
  	
  }

  /**
   * Fetches one or more BatchFiles 
   *
   * @param AvaBatchFileFetch $parameters
   * @return AvaBatchFileFetchResponse
   */
  public function BatchFileFetch(&$fetchRequest) {
  	
  	return $this->client->BatchFileFetch(array('FetchRequest' => $fetchRequest))->getBatchFileFetchResult();
    
  }

  /**
   * Saves a Batch File 
   *
   * @param AvaBatchFileSave $parameters
   * @return AvaBatchFileSaveResponse
   */
  public function BatchFileSave(&$batchFile) {   
  	
  	return $this->client->BatchFileSave(array('BatchFile' => $batchFile))->getBatchFileSaveResult();
  	
  }

  /**
   * Deletes one or more BatchFiles 
   *
   * @param AvaBatchFileDelete $parameters
   * @return AvaBatchFileDeleteResponse
   */
  public function BatchFileDelete(&$deleteRequest) {
    
  	return $this->client->BatchFileDelete(array('DeleteRequest' => $deleteRequest))->getBatchFileDeleteResult();
  	
  }

  /**
   * Tests connectivity and version of the service 
   *
   * @param AvaPing $parameters
   * @return AvaPingResponse
   */
  public function Ping($message = '') {    
      return $this->client->Ping(array('Message' => $message))->getPingResult();
  }

  /**
   * Checks authentication and authorization to one or more operations on the service. 
   *
   * @param AvaIsAuthorized $parameters
   * @return AvaIsAuthorizedResponse
   */
public function IsAuthorized($operations)
    {
        return $this->client->IsAuthorized(array('Operations' => $operations))->getIsAuthorizedResult();
    }

}