<?php
/**
 * AvaAddressServiceSoap.class.phpphp
 */
 
/**
 * Proxy interface for the Avalara Address Web Service. 
 *
 * AvaAddressServiceSoap reads its configuration values from static variables defined
 * in AvaATConfig.class.php. This file must be properly configured with your security credentials.
 *
 * <p>
 * <b>Example:</b>
 * <pre>
 *  $addressService = new AvaAddressServiceSoap();
 * </pre>
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 * 
 */

class AvaAddressServiceSoap extends AvaAvalaraSoapClient
{
    static private $servicePath = '/AvaAddress/AddressSvc.asmx';
    static protected $classmap = array(
        							'Validate' => 'Validate',
                                    'BaseRequest' => 'BaseRequest',
                                    'ValidateRequest' => '\Avalara\AddressSvc\AvaValidateRequest',
                                    'BaseAddress' => 'BaseAddress',
                                    'ValidAddress' => '\Avalara\AddressSvc\AvaValidAddress',
                                    'TextCase' => '\Avalara\AddressSvc\TextCase',
                                    'ValidateResult' => '\Avalara\AddressSvc\AvaValidateResult',
                                    'BaseResult' => 'AvaBaseResult',
                                    'SeverityLevel' => '\Avalara\AddressSvc\AvaSeverityLevel',
                                    'Message' => '\Avalara\BaseSvc\AvaMessage',
                                    'Profile' => 'AvaProfile',
                                    'Ping' => 'AvaPing',
                                    'PingResult' => '\Avalara\BaseSvc\AvaPingResult',
                                    'IsAuthorized' => 'AvaIsAuthorized',
                                    'IsAuthorizedResult' => '\Avalara\BaseSvc\AvaIsAuthorizedResult');
        
    /**
     * Construct a proxy for Avalara's Address Web Service using the default URL as coded in the class or programatically set.
     * 
     * <b>Example:</b>
     * <pre>
     *  $port = new AvaAddressServiceSoap();
     *  $port->ping();
     * </pre>
     *
     * @see AvalaraSoapClient
     * @see AvaTaxServiceSoap
     */

    public function __construct($configurationName = 'Default')
    {
        $config = new AvaATConfig($configurationName);
        $this->client = new AvaDynamicSoapClient   (
            $config->addressWSDL,
            array
            (
                'location' => $config->url.$config->addressService, 
                'trace' => $config->trace,
                'classmap' => AvaAddressServiceSoap::$classmap
            ), 
            $config
        );
    }

    /**
     * Checks authentication of and authorization to one or more
     * operations on the service.
     * 
     * This operation allows pre-authorization checking of any
     * or all operations. It will return a comma delimited set of
     * operation names which will be all or a subset of the requested
     * operation names.  For security, it will never return operation
     * names other than those requested (no phishing allowed).
     * 
     * <b>Example:</b><br>
     * <code> isAuthorized("GetTax,PostTax")</code>
     *
     * @param string $operations  a comma-delimited list of operation names
     *
     * @return AvaIsAuthorizedResult
     * @throws SoapFault
     */

    public function isAuthorized($operations)
    {
        return $this->client->IsAuthorized(array('Operations' => $operations))->IsAuthorizedResult;
    }
    
    /**
     * Verifies connectivity to the web service and returns version
     * information about the service.
     *
     * <b>NOTE:</b>This replaces TestConnection and is available on
     * every service.
     *
     * @param string $message for future use
     * @return AvaPingResult
     * @throws SoapFault
     */

    public function ping($message = '')
    {
        return $this->client->Ping(array('Message' => $message))->PingResult;
    }
    
    /**
     * Validates an address and returns a collection of possible
     * {@link AvaValidAddress} objects in a {@link AvaValidateResult} object.
     * 
     * Takes an {@link AvaAddress}, an optional {@link TextCase}
     * property that determines the casing applied to a validated
     * address.  It defaults to AvaTextCase::$Default.
     * <b>Example:</b><br>
     * <pre>
     *  use Avalara\AddressSvc\AvaAddress as AvaAddress;
     *  $port = new AvaAddressServiceSoap();
     *
     *  $address = new AvaAddress();
     *  $address->setLine1("900 Winslow Way");
     *  $address->setLine2("Suite 130");
     *  $address->setCity("Bainbridge Is");
     *  $address->setRegion("WA");
     *  $address->setPostalCode("98110-2450");
     *
     *  $result = $port->validate(new AvaValidateRequest($address,AvaTextCase::$Upper));
     *  $addresses = $result->validAddresses();
     *  print('Number of addresses returned is: '.sizeof($addresses)); 
     * </pre>
     *
     * @param AvaValidateRequest
     * @return AvaValidateResult
     *
     * @throws SoapFault
     */

	
	 public function validate($validateRequest)
    {
        return $this->client->Validate(array('ValidateRequest' => $validateRequest))->ValidateResult;
    }     


}