<?php
/**
 * AvaPing.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaPing {
        private $Message; // string

        public function setMessage($value){$this->Message=$value;} // string
        public function getMessage(){return $this->Message;} // string

    }

}