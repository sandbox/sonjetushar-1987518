<?php
/**
 * AvaBaseResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBaseResult {
        private $TransactionId; // string
        private $ResultCode; // SeverityLevel
        private $Messages; // ArrayOfMessage

        public function setTransactionId($value){$this->TransactionId=$value;} // string
        public function getTransactionId(){return $this->TransactionId;} // string

        public function setResultCode($value){$this->ResultCode=$value;} // AvaSeverityLevel
        public function getResultCode(){return $this->ResultCode;} // AvaSeverityLevel

        public function setMessages($value){$this->Messages=$value;} // ArrayOfMessage
        public function getMessages(){return $this->Messages;} // ArrayOfMessage

    }

}