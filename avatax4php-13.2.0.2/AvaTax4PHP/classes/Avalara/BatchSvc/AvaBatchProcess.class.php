<?php
/**
 * AvaBatchProcess.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchProcess {
        private $BatchProcessRequest; // BatchProcessRequest

        public function setBatchProcessRequest($value){$this->BatchProcessRequest=$value;} // BatchProcessRequest
        public function getBatchProcessRequest(){return $this->BatchProcessRequest;} // BatchProcessRequest

    }

}