<?php
/**
 * AvaBatchFileDeleteResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFileDeleteResponse {
        private $BatchFileDeleteResult; // DeleteResult

        public function setBatchFileDeleteResult($value){$this->BatchFileDeleteResult=$value;} // DeleteResult
        public function getBatchFileDeleteResult(){return $this->BatchFileDeleteResult;} // DeleteResult

    }

}