<?php
/**
 * AvaBatchDelete.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchDelete {
        private $DeleteRequest; // DeleteRequest

        public function setDeleteRequest($value){$this->DeleteRequest=$value;} // DeleteRequest
        public function getDeleteRequest(){return $this->DeleteRequest;} // DeleteRequest

    }

  }