<?php
/**
 * AvaSeverityLevel.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaSeverityLevel {
        const Success = 'Success';
        const Warning = 'Warning';
        const Error = 'Error';
        const Exception = 'Exception';

    }

}