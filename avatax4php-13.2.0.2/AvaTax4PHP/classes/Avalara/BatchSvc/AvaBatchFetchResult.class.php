<?php
/**
 * AvaBatchFetchResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaBatchFetchResult extends AvaBaseResult {
        private $Batches; // ArrayOfBatch
        private $RecordCount; // int

        public function setBatches($value){$this->Batches=$value;} // ArrayOfBatch
        public function getBatches(){return $this->Batches;} // ArrayOfBatch

        public function setRecordCount($value){$this->RecordCount=$value;} // int
        public function getRecordCount(){return $this->RecordCount;} // int

    }

}