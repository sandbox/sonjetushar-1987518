<?php
/**
 * AvaBatchDeleteResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchDeleteResponse {
        private $BatchDeleteResult; // DeleteResult

        public function setBatchDeleteResult($value){$this->BatchDeleteResult=$value;} // DeleteResult
        public function getBatchDeleteResult(){return $this->BatchDeleteResult;} // DeleteResult

    }

}