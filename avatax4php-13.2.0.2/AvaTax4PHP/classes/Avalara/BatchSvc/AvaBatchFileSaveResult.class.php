<?php
/**
 * AvaBatchFileSaveResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaBatchFileSaveResult extends AvaBaseResult {
        private $BatchFileId; // int

        public function setBatchFileId($value){$this->BatchFileId=$value;} // int
        public function getBatchFileId(){return $this->BatchFileId;} // int

    }

}