<?php
/**
 * AvaGetTaxHistoryResultult.class.php
 */

/**
 * Result data returned from {@link AvaTaxServiceSoap#getTaxHistory} for a previously calculated tax document.
 *
 * @see GetTaxHistoryRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaGetTaxHistoryResult //extends AvaBaseResult
    {
        private $GetTaxRequest;
        private $GetTaxResult;

        /**
         * Gets the original {@link AvaGetTaxRequest} for the document.
         *
         * @return AvaGetTaxRequest
         */

        public function getGetTaxRequest() { return $this->GetTaxRequest; }

        /**
         * Gets the original {@link AvaGetTaxResult} for the document.
         *
         * @return GetTaxResult
         */

        public function getGetTaxResult() { return $this->GetTaxResult; }


// AvaBaseResult innards - work around a bug in SoapClient

        /**
         * @var string
         */
        private $TransactionId;
        /**
         * @var string must be one of the values defined in {@link AvaSeverityLevel}.
         */
        private $ResultCode = 'Success';
        /**
         * @var array of Message.
         */
        private $Messages = array();

        /**
         * Accessor
         * @return string
         */
        public function getTransactionId() { return $this->TransactionId; }
        /**
         * Accessor
         * @return string
         */
        public function getResultCode() { return $this->ResultCode; }
        /**
         * Accessor
         * @return array
         */
        public function getMessages() { return EnsureIsArray($this->Messages->Message); }


    }

}