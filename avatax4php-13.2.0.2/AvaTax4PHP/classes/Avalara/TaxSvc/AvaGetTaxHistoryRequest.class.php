<?php
/**
 * AvaGetTaxHistoryRequest.class.phpphp
 */

/**
 * Data to pass to {@link AvaTaxServiceSoap#getTaxHistory}.
 * <p>
 * The request must specify all of CompanyCode, DocCode, and DocType in order to uniquely identify the document.
 * </p>
 *
 * @see GetTaxHistoryResult
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaGetTaxHistoryRequest extends AvaTaxRequest
    {
        private $DetailLevel;

        public function __construct()
        {
            parent::__construct();
            $this->DetailLevel = AvaDetailLevel::$Document;  // this is right Document
            $this->DocType = AvaDocumentType::$SalesOrder;  // this is right Document

        }

        /**
         * Specifies the level of detail to return.
         *
         * @return AvaDetailLevel
         * @var string
         * @see DetailLevel
         */

        public function getDetailLevel() { return $this->DetailLevel; }

        /**
         * Specifies the level of detail to return.
         *
         * @see DetailLevel
         * @return string
         */

        public function setDetailLevel($value) { AvaDetailLevel::Validate($value); $this->DetailLevel = $value; return $this; }			//Summary or Document or Line or Tax or Diagnostic - enum

    }
}