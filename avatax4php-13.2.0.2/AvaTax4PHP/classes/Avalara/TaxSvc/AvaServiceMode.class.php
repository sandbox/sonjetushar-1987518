<?php
/**
 * AvaServiceModeode.class.php
 */

/**
 * Specifies the AvaServiceMode.
 *
 * @see GetTaxRequest, GetTaxHistoryRequest
 *
 * This is only supported by AvaLocal servers. It provides the ability to controls whether tax is calculated locally or remotely when using an AvaLocal server.
 * The default is Automatic which calculates locally unless remote is necessary for non-local addresses
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaServiceMode extends AvaEnum
    {
        /**
         * Automated handling by local and/or remote server.
         */
        public static $Automatic = "Automatic";


        /**
         * AvaLocal server only. Lines requiring remote will not be calculated.
         */
        public static $Local = "Local";

        /**
         * All lines are calculated by AvaTax remote server.
         */
        public static $Remote = "Remote";

        public static function Values()
        {
            return array(
                AvaServiceMode::$Automatic,
                AvaServiceMode::$Local,
                AvaServiceMode::$Remote
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }
}