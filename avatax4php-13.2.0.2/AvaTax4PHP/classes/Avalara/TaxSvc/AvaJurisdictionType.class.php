<?php
/**
 * AvaJurisdictionTypeype.class.php
 */

/**
 * A Jurisdiction Type describes the jurisdiction for which a particular tax is applied to a document.
 * Jurisdiction is determined by the AvaGetTaxRequest#getDestinationAddress or AvaGetTaxRequest#getOriginAddress of a AvaGetTaxRequest.
 * Multiple jurisdictions might be applied to a single Line during a tax calcuation.
 * Details are available in the AvaTaxDetail of the AvaGetTaxResult.
 *
 * @see TaxDetail
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaJurisdictionType
    {
        /**
         *  Unspecified Jurisdiction
         *
         * @var AvaJurisdictionType
         */
        public static $Composite	= 'Composite';

        /**
         * State
         *
         * @var AvaJurisdictionType
         */
        public static $State	= 'State';

        /**
         * County
         *
         * @var AvaJurisdictionType
         */
        public static $County	= 'County';

        /**
         * City
         *
         * @var AvaJurisdictionType
         */
        public static $City		= 'City';

        /**
         * Special Tax Jurisdiction
         *
         * @var AvaJurisdictionType
         */
        public static $Special	= 'Special';
        /*

            public static function Values()
            {
                return array(
                    AvaJurisdictionType::$Composite,
                    AvaJurisdictionType::$State,
                    AvaJurisdictionType::$County,
                    AvaJurisdictionType::$City,
                    AvaJurisdictionType::$Special
                );
            }

            // Unfortunate boiler plate due to polymorphism issues on static functions
            public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

           */
    }
}