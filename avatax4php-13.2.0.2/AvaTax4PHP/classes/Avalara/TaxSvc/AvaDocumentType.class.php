<?php
/**
 * AvaDocumentTypeype.class.php
 */

/**
 * The document type specifies the category of the document and affects how the document
 * is treated after a tax calculation. Specified when constructing a {@link AvaGetTaxRequest}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */

namespace Avalara\TaxSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaDocumentType extends AvaEnum
    {

        /**
         * Sales Order, estimate or quote.
         *
         * @var AvaDocumentType
         */
        public static $SalesOrder		= 'SalesOrder';

        /**
         *   The document is a permanent invoice; document and tax calculation results are saved in the tax history.
         *
         * @var AvaDocumentType
         */
        public static $SalesInvoice		= 'SalesInvoice';

        /**
         *  Purchase order, estimate, or quote.
         *
         * @var AvaDocumentType
         */
        public static $PurchaseOrder	= 'PurchaseOrder';

        /**
         *  The document is a permanent invoice; document and tax calculation results are saved in the tax history.
         *
         * @var AvaDocumentType
         */
        public static $PurchaseInvoice	= 'PurchaseInvoice';

        /**
         *Sales Return Order.
         *
         * @var AvaDocumentType
         */
        public static $ReturnOrder		= 'ReturnOrder';

        /**
         * The document is a permanent sales return invoice; document and tax calculation results are saved in the tax history AvaGetTaxResult will return with a AvaDocStatus of Saved.
         *
         * @var AvaDocumentType
         */
        public static $ReturnInvoice	= 'ReturnInvoice';

        /**
         * Inventory Transfer Order
         *
         * @var AvaDocumentType
         */
        public static $InventoryTransferOrder	= 'InventoryTransferOrder';

        /**
         * Inventory Transfer Invoice
         *
         * @var AvaDocumentType
         */
        public static $InventoryTransferInvoice	= 'InventoryTransferInvoice';

        /**
         * This will return all types of documents.
         *
         * @var AvaDocumentType
         */
        public static $Any	= 'Any';

        public static function Values()
        {
            return array(
                AvaDocumentType::$SalesOrder,
                AvaDocumentType::$SalesInvoice,
                AvaDocumentType::$PurchaseOrder,
                AvaDocumentType::$PurchaseInvoice,
                AvaDocumentType::$ReturnOrder,
                AvaDocumentType::$ReturnInvoice,
                AvaDocumentType::$InventoryTransferOrder,
                AvaDocumentType::$InventoryTransferInvoice,
                AvaDocumentType::$Any
            );
        }
        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}
