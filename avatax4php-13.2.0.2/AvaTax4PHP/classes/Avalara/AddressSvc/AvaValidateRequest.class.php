<?php
/**
 * AvaValidateRequest.class.php
 */

/**
 * Data wrapper used internally to pass arguments within {@link AvaAddressServiceSoap#validate}. End users should not need to use this class.
 *
 * <pre>
 * <b>Example:</b>
 * use Avalara\AddressSvc\AvaAddress as AvaAddress;
 * use Avalara\AddressSvc\AvaValidateRequest as AvaValidateRequest;
 * use Avalara\AddressSvc\AvaValidateResult as AvaValidateResult;
 * $svc = new AvaAddressServiceSoap();
 *
 * $address = new AvaAddress();
 * $address->setLine1("900 Winslow Way");
 * $address->setCity("Bainbridge Island");
 * $address->setRegion("WA");
 * $address->setPostalCode("98110");
 *
 * ValidateRequest validateRequest = new AvaValidateRequest();
 * validateRequest.setAddress(address);
 * validateRequest.setTextCase(TextCase.Upper);
 *
 * AvaValidateResult result = svc.validate(validateRequest);
 * ArrayOfValidAddress arrValids = result.getValidAddresses();
 * int numAddresses = (arrValids == null ||
 *         arrValids.getValidAddress() == null ? 0 :
 *         arrValids.getValidAddress().length);
 * System.out.println("Number of Addresses is " + numAddresses);
 * </pre>
 *
 * @author    Avalara
 * @copyright 2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AddressSvc
 */

//public function validate($address, $textCase = 'Default', $coordinates = false)
//{
// $request = new AvaValidateRequest($address, ($textCase ? $textCase : AvaTextCase::$Default), $coordinates);
// return $this->client->Validate(array('ValidateRequest' => $request))->ValidateResult;
//}

namespace Avalara\AddressSvc {
    class AvaValidateRequest
    {
        private $Address;
        private $TextCase = 'Default';
        private $Coordinates = false;
        private $Taxability=false;

        public function __construct($address = null, $textCase = 'Default', $coordinates = false)
        {
            $this->setAddress($address);
            $this->setTextCase($textCase);
            $this->setCoordinates($coordinates);
        }

        // mutators
        /**
         * The address to Validate.
         * <pre>
         * <b>Example:</b>
         * $address = new AvaAddress();
         * $address->setLine1("900 Winslow Way");
         * $address->setCity("Bainbridge Island");
         * $address->setRegion("WA");
         * $address->setPostalCode("98110");
         *
         * $validateRequest = new AvaValidateRequest();
         * $validateRequest->setAddress(address);
         * $validateRequest->setTextCase(AvaTextCase::$Upper);
         *
         * $result = svc->validate(validateRequest);
         * </pre>
         *
         * @var AvaAddress
         */

        public function setAddress(&$value) { $this->Address = $value; return $this; }

        /**
         * The casing to apply to the validated address(es).
         * <pre>
         * <b>Example:</b>
         * <b>Example:</b>
         * $address = new AvaAddress();
         * $address->setLine1("900 Winslow Way");
         * $address->setCity("Bainbridge Island");
         * $address->setRegion("WA");
         * $address->setPostalCode("98110");
         *
         * $validateRequest = new AvaValidateRequest();
         * $validateRequest->setAddress(address);
         * $validateRequest->setTextCase(AvaTextCase::$Upper);
         *
         * $result = svc->validate(validateRequest);
         * </pre>
         *
         * @var string
         * @see TextCase
         */

        public function setTextCase($value)
        {
            if($value)
            {
                AvaTextCase::Validate($value);
                $this->TextCase = $value;
            }
            else
            {
                $this->TextCase = AvaTextCase::$Default;
            }
            return $this;
        }

        /**
         * Sets whether to fetch the coordinates value for this ValidateRequest.
         *  <p>
         *  True will return the @see ValidAddress#Latitude and @see AvaValidAddress#Longitude values for the @see ValidAddresses
         *  Default value is <b>false</b>
         *  </p>
         * @var boolean
         */
        public function setCoordinates($value) { $this->Coordinates = ($value ? true : false); return $this; }


        //@author:swetal
        public function setTaxability($value)
        {
            $this->Taxability=$value;
        }
        // accessors
        /**
         * The address to Validate.
         * <pre>
         * <b>Example:</b>
         * $address = new AvaAddress();
         * $address->setLine1("900 Winslow Way");
         * $address->setCity("Bainbridge Island");
         * $address->setRegion("WA");
         * $address->setPostalCode("98110");
         *
         * $validateRequest = new AvaValidateRequest();
         * $validateRequest->setAddress(address);
         * $validateRequest->setTextCase(AvaTextCase::$Upper);
         *
         * $result = svc->validate(validateRequest);
         * </pre>
         *
         * @return AvaAddress
         */

        public function getAddress() { return $this->Address; }

        /**
         * The casing to apply to the validated address(es).
         * <pre>
         * <b>Example:</b>
         * <b>Example:</b>
         * $address = new AvaAddress();
         * $address->setLine1("900 Winslow Way");
         * $address->setCity("Bainbridge Island");
         * $address->setRegion("WA");
         * $address->setPostalCode("98110");
         *
         * $validateRequest = new AvaValidateRequest();
         * $validateRequest->setAddress(address);
         * $validateRequest->setTextCase(AvaTextCase::$Upper);
         *
         * $result = svc->validate(validateRequest);
         * </pre>
         *
         * @return string
         * @see TextCase
         */

        public function getTextCase() { return $this->TextCase; }

        /**
         * Returns whether to return the coordinates value for this ValidateRequest.
         *  <p>
         *  True will return the @see ValidAddress#Latitude and @see AvaValidAddress#Longitude values for the @see ValidAddresses
         *  Default value is <b>false</b>
         *  </p>
         * @return boolean
         */

        public function getCoordinates() { return $this->Coordinates; }
    }
}