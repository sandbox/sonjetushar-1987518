<?php
/**
 * TextCase.class.php
 */

/**
 * The casing to apply to the valid address(es) returned in the validation result.
 *
 * @author    Avalara
 * @copyright 2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AddressSvc
 */
namespace Avalara\AddressSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaTextCase extends AvaEnum
    {
        public static $Default 	= 'Default';
        public static $Upper 	= 'Upper';
        public static $Mixed 	= 'Mixed';

        public static function Values()
        {
            return array(
                AvaTextCase::$Default,
                AvaTextCase::$Upper,
                AvaTextCase::$Mixed,
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }

}