<?php
/**
 * AvaCustomerSaveRequest.class.php
 */

/**
 * Input for {@link CustomerSave}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    class AvaCustomerSaveRequest {
        private $CompanyCode; // string
        private $Customer; // Customer

        public function setCompanyCode($value){$this->CompanyCode=$value;} // string

        /**
         * Company Code of the company to which the customer belongs.
         */
        public function getCompanyCode(){return $this->CompanyCode;} // string

        public function setCustomer($value){$this->Customer=$value;} // Customer

        /**
         * The customer to add.
         */
        public function getCustomer(){return $this->Customer;} // Customer

    }

}