<?php
/**
 * AvaCertificateImageGetResult.class.php
 */

/**
 * Contains the get certificate image operation result returned by {@link CertificateImageGet}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaCertificateImageGetResult extends AvaBaseResult {
        private $AvaCertId; // string
        private $Image; // base64Binary

        /**
         * Unique identifier for the Certificate record.
         */
        public function getAvaCertId(){return $this->AvaCertId;} // string

        /**
         * Certificate image.
         */
        public function getImage(){return $this->Image;} // base64Binary

    }

}