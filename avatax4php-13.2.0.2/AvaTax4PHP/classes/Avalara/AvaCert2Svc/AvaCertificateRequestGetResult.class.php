<?php
/**
 * AvaCertificateRequestGetResult.class.php
 */

/**
 * Contains the get certificate request operation result returned by {@link CertificateRequestGet}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaCertificateRequestGetResult extends AvaBaseResult {
        private $CertificateRequests; // ArrayOfCertificateRequest

        /**
         * CertificateRequests contains collection of certificate requests.
         */
        public function getCertificateRequests()
        {
            if(isset($this->CertificateRequests->CertificateRequest))
            {
                return EnsureIsArray($this->CertificateRequests->CertificateRequest);
            }
            else
            {
                return null;
            }
        } // ArrayOfCertificateRequest

    }

}