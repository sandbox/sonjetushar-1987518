<?php
/**
 * AvaRequestType.class.php
 */

/**
 * AvaRequestType indicates the type of the request to be initiated.
 * @see InitiateExemptCertRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaRequestType extends AvaEnum
    {
        /**
         *  Standard sends correspondences and follow ups related to the Request to the associated Customer.
         *
         * @var AvaRequestType
         */
        public static $STANDARD	= 'STANDARD';

        /**
         *  Direct does not send any correspondence or follow ups related to the Request to the associated Customer.
         *
         * @var AvaRequestType
         */
        public static $DIRECT	= 'DIRECT';

        public static function Values()
        {
            return array(
                AvaRequestType::$STANDARD,
                AvaRequestType::$DIRECT
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}