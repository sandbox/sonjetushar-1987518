<?php

// AvaATConfig object is how credentials are set
// Tax or Address Service Objects take an argument
// which is the name of the AvaATConfig object ('Test' or 'Prod' below)


/* This is a configuration called 'Development'. 
 * Only values different from 'Default' need to be specified.
 * Example:
 *
 * $service = new AvaAddressServiceSoap('Development');
 * $service = new AvaTaxServiceSoap('Development');
 */
new AvaATConfig('Development', array(
    'url'       => 'https://development.avalara.net',
    'account'   => '<Your Production Account Here>',
    'license'   => '<Your Production License Key Here>',
    'trace'     => true) // change to false for production
);

/* This is a configuration called 'Production' 
 * Example:
 *
 * $service = new AvaAddressServiceSoap('Production');
 * $service = new AvaTaxServiceSoap('Production');
 */
new AvaATConfig('Production', array(
    'url'       => 'https://avatax.avalara.net',
    'account'   => '<Your Production Account Here>',
    'license'   => '<Your Production License Key Here>',
    'trace'     => false) // change to false for production
    // To enable "Jaas" profile add one more array element : 'name' => 'Jaas' 
);

/* This is Jaas enabled Developement Configuration called 'JaasDevelopement' 
 * Example:
 *
 * $service = new AvaAddressServiceSoap('JaasDevelopement');
 * $service = new AvaTaxServiceSoap('JaasDevelopement');
 */
new AvaATConfig('JaasDevelopement', array(
    'url'       => 'https://development.avalara.net',
    'account'   => '<Your Production Account Here>',
    'license'   => '<Your Production License Key Here>',
    'name'      => 'Jaas',  // If Jaas is not enabled then remove this element  
    'trace'     => true)    // change to false for production
);


?>
