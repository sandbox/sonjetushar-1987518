<?php

    require_once('../simpletest/autorun.php');
    require('../AvaTax4PHP/AvaTax.php');	// include in all Avalara Scripts
    require('Credentials.php');	// where service URL, account, license key are set
    use Avalara\BaseSvc\AvaSeverityLevel as AvaSeverityLevel;
    use Avalara\TaxSvc\AvaDetailLevel as AvaDetailLevel;
    use Avalara\TaxSvc\AvaDocStatus as AvaDocStatus;
    use Avalara\TaxSvc\AvaDocumentType as AvaDocumentType;
    use Avalara\TaxSvc\AvaGetTaxRequest as AvaGetTaxRequest;
    use Avalara\TaxSvc\AvaTaxOverrideType as AvaTaxOverrideType;
    use Avalara\TaxSvc\AvaServiceMode as AvaServiceMode;
    use Avalara\AddressSvc\AvaAddress as AvaAddress;
    use Avalara\TaxSvc\AvaLine as AvaLine;
    use Avalara\TaxSvc\AvaJurisdictionType as AvaJurisdictionType;
    use Avalara\TaxSvc\AvaPostTaxRequest as AvaPostTaxRequest;
    use Avalara\TaxSvc\AvaGetTaxHistoryRequest as AvaGetTaxHistoryRequest;
    use Avalara\TaxSvc\AvaCommitTaxRequest as AvaCommitTaxRequest;
    use Avalara\TaxSvc\AvaCancelTaxRequest as AvaCancelTaxRequest;
    use Avalara\TaxSvc\AvaCancelCode as AvaCancelCode;
    use Avalara\TaxSvc\AvaAdjustTaxRequest as AvaAdjustTaxRequest;
    use Avalara\TaxSvc\AvaTaxOverride as AvaTaxOverride;
    use Avalara\TaxSvc\AvaApplyPaymentRequest as AvaApplyPaymentRequest;
    use Avalara\TaxSvc\AvaReconcileTaxHistoryRequest as AvaReconcileTaxHistoryRequest;




    class TaxSvcTest extends UnitTestCase
    {
		private $getTaxResult;
        private $getTaxRequest;
        private $getTaxHistoryRequest;
		private $getTaxHistoryResult;
		private $client;
		private $postResult;
		private $commitResult;

		public function __construct()
		{
		    global $client;

		    $client=new AvaTaxServiceSoap('Development');

		}
		function testPing()
        {
            global $client;
	    	$result = $client->ping("");

            $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());

        }
	    function testIsAuthorized()
        {
            global $client;
	    	$result = $client->isAuthorized("Ping,IsAuthorized,Validate");
            $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
        }
		function testGetTax()
		{
		    global $getTaxResult,$getTaxRequest,$client;

		    $dateTime=new DateTime();
		    $docCode= "PHP".date_format($dateTime,"dmyGis");

		    $getTaxRequest=$this->CreateTaxRequest($docCode);
		    $getTaxResult = $client->getTax($getTaxRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success,$getTaxResult->getResultCode());
		    $this->assertEqual(AvaDocStatus::$Saved,$getTaxResult->getDocStatus());

	        $this->assertEqual(1010,$getTaxResult->getTotalAmount());
	        $this->assertEqual(95.95,$getTaxResult->getTotalTax());


		    //AvaTaxDetail
		    $this->assertEqual(count($getTaxRequest->getLines()),count($getTaxResult->getTaxLines()),"Tax line count");

            $taxLineArray=$getTaxResult->getTaxLines();
            $taxLine=$taxLineArray[0];

            $taxDetailArray=$taxLine->getTaxDetails();
            $taxDetail=$taxDetailArray[0];

            $this->assertEqual(53,$taxDetail->getJurisCode());
            $this->assertEqual(AvaJurisdictionType::$State,$taxDetail->getJurisType());
            $this->assertEqual(1000,$taxDetail->getTaxable());
            $this->assertEqual(0.065000,$taxDetail->getRate());
            $this->assertEqual(65,$taxDetail->getTax());
            $this->assertEqual(0,$taxDetail->getNonTaxable());

		}
	    function testTaxHistory()
        {
            global $getTaxResult,$getTaxHistoryRequest,$getTaxRequest,$getTaxHistoryResult,$client;

            $getTaxHistoryRequest = new AvaGetTaxHistoryRequest();
            $getTaxHistoryRequest->setCompanyCode($getTaxRequest->getCompanyCode());
            $getTaxHistoryRequest->setDocCode($getTaxRequest->getDocCode());
            $getTaxHistoryRequest->setDetailLevel(AvaDetailLevel::$Diagnostic);
            $getTaxHistoryRequest->setDocType(AvaDocumentType::$SalesInvoice);

	    	$getTaxHistoryResult = $client->getTaxHistory($getTaxHistoryRequest);


            $this->assertEqual("Success",$getTaxHistoryResult->getResultCode());

            $historyTaxRequest = $getTaxHistoryResult->getGetTaxRequest();
            $this->assertNotNull($historyTaxRequest);
            $this->assertEqual(count($getTaxRequest->getLines()), count($historyTaxRequest->getLines()));
		    $this->assertEqual(count($getTaxRequest->getAddresses()), count($historyTaxRequest->getAddresses()));

		    //compare all properties
		    $this->CompareHistory($getTaxRequest,$getTaxResult,$getTaxHistoryResult);

        }
		function testPostTax()
		{
		    global $getTaxRequest,$getTaxResult,$client,$postResult;

		    //Post tax
		    $postRequest = new AvaPostTaxRequest();

		    $postRequest->setCompanyCode($getTaxRequest->getCompanyCode());
		    $postRequest->setDocType($getTaxRequest->getDocType());
		    $postRequest->setDocCode($getTaxRequest->getDocCode());
		    $postRequest->setDocDate($getTaxRequest->getDocDate());
		    $postRequest->setTotalAmount($getTaxResult->getTotalAmount());
		    $postRequest->setTotalTax($getTaxResult->getTotalTax());

		    $postResult = $client->postTax($postRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $postResult->getResultCode());

		}
		function testCommitTax()
		{
		    global $postResult,$getTaxRequest,$client,$commitResult;

		    $commitRequest = new AvaCommitTaxRequest();
		    $commitRequest->setCompanyCode($getTaxRequest->getCompanyCode());
		    $commitRequest->setDocCode($getTaxRequest->getDocCode());
		    $commitRequest->setDocType($getTaxRequest->getDocType());

		    $commitResult = $client->commitTax($commitRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $commitResult->getResultCode());

		}
		function testCancelTax()
		{
		    global $getTaxRequest,$commitResult,$client;

		    $cancelRequest = new AvaCancelTaxRequest();

		    $cancelRequest->setCompanyCode($getTaxRequest->getCompanyCode());
		    $cancelRequest->setDocCode($getTaxRequest->getDocCode());
		    $cancelRequest->setDocType($getTaxRequest->getDocType());
		    $cancelRequest->setCancelCode(AvaCancelCode::$DocDeleted);

		    $cancelResult = $client->cancelTax($cancelRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $cancelResult->getResultCode());

		}
		function testAdjustTax()
		{
			global $client;

			$dateTime=new DateTime();
		    $docCode= "PHPAdjustTaxTest".date_format($dateTime,"dmyGis");
			$getTaxRequest=$this->CreateTaxRequest($docCode);
			$getTaxRequest->setCommit(TRUE);

			$getTaxResult= $client->getTax($getTaxRequest);
			$this->assertEqual(AvaSeverityLevel::$Success,$getTaxResult->getResultCode());

			$adjustTaxRequest=new AvaAdjustTaxRequest();
			$adjustTaxRequest->setAdjustmentReason(8);
			$adjustTaxRequest->setAdjustmentDescription("For testing");

			$getTaxRequest->getLine("1")->setAmount(2000);
			$adjustTaxRequest->setGetTaxRequest($getTaxRequest);

			$adjustTaxResult= $client->adjustTax($adjustTaxRequest);

			$this->assertEqual(AvaSeverityLevel::$Success,$adjustTaxResult->getResultCode());

			$var=$adjustTaxResult->getTotalTax();

			$cancelRequest = new AvaCancelTaxRequest();
		    $cancelRequest->setDocCode($getTaxRequest->getDocCode());
		    $cancelRequest->setDocType($getTaxRequest->getDocType());
		    $cancelRequest->setCompanyCode($getTaxRequest->getCompanyCode());
		    $cancelRequest->setCancelCode(AvaCancelCode::$AdjustmentCancelled);

		    $cancelResult = $client->cancelTax($cancelRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $cancelResult->getResultCode());

		    //Change status to DocDeleted and call cancel again
		    $cancelRequest->setCancelCode(AvaCancelCode::$DocDeleted);
		    $cancelResult = $client->cancelTax($cancelRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $cancelResult->getResultCode());



		}
		function testTaxOverrideHeader()
		{
		    global $client;

		    $dateTime=new DateTime();

		    $request = $this->CreateTaxRequest("TaxOverrideTest".date_format($dateTime,"dmyGis"));

		    $taxOverride = new AvaTaxOverride();
		    $taxOverride->setTaxOverrideType(AvaTaxOverrideType::$TaxAmount);
		    $taxOverride->setTaxAmount(5);
		    $dateTime=new DateTime();
		    $taxOverride->setTaxDate(date_format($dateTime,"Y-m-d"));
		    $taxOverride->setReason("Return");

		    $request->setTaxOverride($taxOverride);

		    $result = $client->getTax($request);

		    $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());

		    $this->assertEqual($result->getTotalTax(),5);
		    $this->assertEqual($result->getTotalTaxCalculated(),95.95);

		}
		function testTaxOverrideLine()
		{
		    global $client;


		    $dateTime=new DateTime();
		    $docCode= "TaxOverrideLineTest".date_format($dateTime,"dmyGis");
		    $request = $this->CreateTaxRequestForTaxOverride($docCode);

		    $result = $client->getTax($request);
		    $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
		    $this->assertEqual($result->getTotalTax(),5);


		    $taxLine=$result->getTaxLines();
		    $taxLine=$taxLine[0];
		    $this->assertEqual($taxLine->getTax(),5);
		    $this->assertEqual($taxLine->getTaxCalculated(),80);


		}
		function testApplyPayment()
		{

		    global $client;

		    $dateTime=new DateTime();
		    $docCode= "ApplyPaymentTest".date_format($dateTime,"dmyGis");
		    $request = $this->CreateTaxRequest($docCode);

		    $request->setDetailLevel(AvaDetailLevel::$Tax);

		    $dateTime->modify("-2 day");
		    $request->setDocDate(date_format($dateTime,"Y-m-d"));

		    $request->setCommit(TRUE);
		    $result = $client->getTax($request);

		    $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());

		    $applyPaymentRequest = new AvaApplyPaymentRequest();
		    $applyPaymentRequest->setCompanyCode($request->getCompanyCode());
		    $applyPaymentRequest->setDocCode($request->getDocCode());
		    $dateTime=new DateTime();
		    $applyPaymentRequest->setPaymentDate(date_format($dateTime,"Y-m-d"));
		    $applyPaymentRequest->setDocType(AvaDocumentType::$SalesInvoice);
		    $applyPaymentResult = $client->applyPayment($applyPaymentRequest);


		    if(AvaSeverityLevel::$Warning==$applyPaymentResult->getResultCode())
		    {
				$message=$applyPaymentResult->getMessages();
				$message=$message[0];
				$message=$message->getName();
				$this->assertEqual("ApplyPaymentWarning",$message);
		    }
		    else
		    {
				$this->assertEqual(AvaSeverityLevel::$Success,$applyPaymentResult->getResultCode());
		    }

		}
		function testReconcileTaxHistory()
		{
			global $client;

			$dateTime=new DateTime();
		    $docCode= "PHPReconcile".date_format($dateTime,"dmyGis");
		    $getTaxRequest=$this->CreateTaxRequest($docCode);
		    $getTaxRequest->setCommit(TRUE);
		    $getTaxResult = $client->getTax($getTaxRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success,$getTaxResult->getResultCode());
		    $this->assertEqual(AvaDocStatus::$Committed,$getTaxResult->getDocStatus());

		    $reconcileTaxHistoryRequest = new AvaReconcileTaxHistoryRequest();
	        $reconcileTaxHistoryRequest->setCompanyCode("DEFAULT");
	        //request.setReconciled(false);
	        $reconcileTaxHistoryRequest->setStartDate(date_format($dateTime,"Y-m-d"));
	        $reconcileTaxHistoryRequest->setEndDate(date_format($dateTime,"Y-m-d"));
	        $reconcileTaxHistoryRequest->setDocStatus(AvaDocStatus::$Committed);
	        $reconcileTaxHistoryRequest->setLastDocCode("0");
	        $reconcileTaxHistoryRequest->setPageSize(1000);
	        $reconcileTaxHistoryRequest->setDocType(AvaDocumentType::$SalesInvoice);

	        //Calling reconHistory Method

	        $reconcileTaxHistoryResult = $client->reconcileTaxHistory($reconcileTaxHistoryRequest);
	        $this->assertEqual(AvaSeverityLevel::$Success,$reconcileTaxHistoryResult->getResultCode());
	        $taxResults = $reconcileTaxHistoryResult->getGetTaxResults();
	        $this->assertTrue(count($taxResults) > 0,"Expected > 0 reconcile records");
	        $this->assertTrue($reconcileTaxHistoryResult->getRecordCount() >= count($taxResults),"RecordCount has to be equal to or more than number of records fetched");
	        $found = FALSE;

	        /*do
	        {
	            foreach ($taxResults as $taxResult)
	            {
	                $this->assertEqual(AvaDocStatus::$Committed, $taxResult->getDocStatus());

	                if (strcmp($taxResult->getDocCode(),$getTaxRequest->getDocCode()) == 0)
	                {
	                    $found = true;
	                }
	            }

	            $reconcileTaxHistoryRequest->setLastDocCode($reconcileTaxHistoryResult->getLastDocCode());
	            $reconcileTaxHistoryResult = $client->reconcileTaxHistory($reconcileTaxHistoryRequest);
	            $taxResults = $reconcileTaxHistoryResult->getGetTaxResults();
	        }
	        while ( count($taxResults) > 0);*/

	        //$this->assertTrue($found,"ReconcileCommittedTest doc not found");

	        //Cancel Tax
	        $cancelRequest = new AvaCancelTaxRequest();

		    $cancelRequest->setCompanyCode($getTaxRequest->getCompanyCode());
		    $cancelRequest->setDocCode($getTaxRequest->getDocCode());
		    $cancelRequest->setDocType($getTaxRequest->getDocType());
		    $cancelRequest->setCancelCode(AvaCancelCode::$DocDeleted);

		    $cancelResult = $client->cancelTax($cancelRequest);

		    $this->assertEqual(AvaSeverityLevel::$Success, $cancelResult->getResultCode());


		}
	    function testTaxIncluded()
	    {
	        global $client;
	        $dateTime=new DateTime();
	    	$docCode= "PHPtestTaxIncluded".date_format($dateTime,"dmyGis");
	    	$request = $this->CreateTaxRequest($docCode);
	    	$lines=$request->getLines();
	        $line = $lines[0];
	        $line->setTaxIncluded(TRUE);

	        $result = $client->getTax($request);

	        $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
	        $this->assertEqual(AvaDocStatus::$Saved, $result->getDocStatus());
	        //$this->assertEqual($result->isReconciled());
	        $this->assertEqual(923.24, $result->getTotalAmount());
	        $this->assertEqual(87.71, $result->getTotalTax());
	        $taxLines= $result->getTaxLines();
	        $taxLine= $taxLines[0];
	        $this->assertTrue($taxLine->getTaxIncluded());


	        // Check tax history
	        $taxHistoryRequest = new AvaGetTaxHistoryRequest();
	        $taxHistoryRequest->setCompanyCode($request->getCompanyCode());
	        $taxHistoryRequest->setDocCode($request->getDocCode());
	        $taxHistoryRequest->setDocType($request->getDocType());
	        $taxHistoryRequest->setDetailLevel(AvaDetailLevel::$Diagnostic);
	        $taxHistoryResult = $this->waitForTaxHistory($taxHistoryRequest, $result->getTimestamp());

	        $this->assertEqual(AvaSeverityLevel::$Success, $taxHistoryResult->getResultCode());
	        $this->compareHistory($request, $result, $taxHistoryResult);
	    }
	function testTaxOverrideType()
   {
       global $client;

       $dateTime=new DateTime();
       $docCode= "TaxOverrideLineTypeTest".date_format($dateTime,"dmyGis");
       $request = $this->CreateTaxRequestForTaxOverrideType($docCode);

       $result = $client->getTax($request);
       $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
       $this->assertEqual($request->getDocType(),$result->getDocType());
       $this->assertEqual($result->getTotalTaxCalculated(),21.1);
   }
    function testGetTaxWithDocType()
   {
        global $client;

        $docType = AvaDocumentType::$InventoryTransferInvoice;
        $docType1 = AvaDocumentType::$InventoryTransferOrder;

        //Testing Document Type InventoryTransferInvoice.
        $request = $this->CreateTaxRequestDocType($docType);

        $result = $client->getTax($request);
        $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
        $this->assertEqual($result->getTotalAmount(),1010);
        $this->assertEqual($result->getTotalTax(),95.95);

        //Testing Document Type InventoryTransferOrder.
        $request = $this->CreateTaxRequestDocType($docType1);

        $result = $client->getTax($request);
        $this->assertEqual(AvaSeverityLevel::$Success, $result->getResultCode());
        $this->assertEqual($result->getTotalAmount(),1010);
        $this->assertEqual($result->getTotalTax(),95.95);
    }

    function testGetTaxBusinessIdentificationNo()
    {
        global $getTaxResult,$getTaxRequest,$client;

        $getTaxRequest=$this->CreateTaxRequestForBINo("HL123");

        $getTaxResult = $client->getTax($getTaxRequest);

        $this->assertEqual(AvaSeverityLevel::$Success,$getTaxResult->getResultCode());
        $this->assertEqual(1010,$getTaxResult->getTotalAmount());
        $this->assertEqual(95.95,$getTaxResult->getTotalTax());
        // Check tax history
        $taxHistoryRequest = new AvaGetTaxHistoryRequest();
        $taxHistoryRequest->setCompanyCode($getTaxRequest->getCompanyCode());
        $taxHistoryRequest->setDocCode($getTaxRequest->getDocCode());
        $taxHistoryRequest->setDocType($getTaxRequest->getDocType());
        $taxHistoryRequest->setDetailLevel(AvaDetailLevel::$Tax);
        $taxHistoryResult = $this->waitForTaxHistory($taxHistoryRequest, $getTaxResult->getTimestamp());
        $this->assertEqual(AvaSeverityLevel::$Success, $taxHistoryResult->getResultCode());
        $this->assertEqual($taxHistoryResult->getGetTaxRequest()->getBusinessIdentificationNo(),$getTaxRequest->getBusinessIdentificationNo());
        for ($i=0;$i< count($getTaxRequest->getLines());$i++)
        {

            $requestLine =$getTaxRequest->getLines();
            $requestLine=$requestLine[$i];

            $historyLine = $taxHistoryResult->getGetTaxRequest()->getLine($requestLine->getNo());
            $this->assertEqual($requestLine->getNo(), $historyLine->getNo());
            $this->assertEqual($requestLine->getAmount(), $historyLine->getAmount());
            $this->assertEqual($requestLine->getItemCode(),$historyLine->getItemCode());
            $this->assertEqual($requestLine->getBusinessIdentificationNo(),$historyLine->getBusinessIdentificationNo());
        }

    }

     function testTaxDetailStateAssignedNo()
    {

        global $client;
    	$getTaxRequest = $this->CreateTaxRequest("testStateAssignedNo");
        $getTaxRequest->setDetailLevel(AvaDetailLevel::$Diagnostic);

        //Set origin Address
	    $origin = new AvaAddress();
	    $origin->setLine1("Avalara");
	    $origin->setLine2("900 winslow way");
	    $origin->setLine3("Suite 100");
	    $origin->setCity("Bainbridge Island");
	    $origin->setRegion("WA");
	    $origin->setPostalCode("98110-1896");
	    $origin->setCountry("USA");
	    $getTaxRequest->setOriginAddress($origin);

	    //Set destination address
	    $destination=  new AvaAddress();
	    $destination->setLine1("400 Embassy Row NE Ste 580");
	    $destination->setCity("Atlanta");
	    $destination->setRegion("GA");
	    $destination->setPostalCode("30328-7000");
	    $destination->setCountry("USA");
	    $getTaxRequest->setDestinationAddress($destination);


        //StateAssignedNo returned by GetTax
        $getTaxResult=$client->getTax($getTaxRequest);
        $this->assertEqual(AvaSeverityLevel::$Success,$getTaxResult->getResultCode());


        $isStateAssignedNo = FALSE;
		$resultTaxDetail=$getTaxResult->getTaxSummary();
        if ( count($resultTaxDetail)  > 0)
        {
            $taxDetail;
            for($i=0;$i<count($resultTaxDetail);$i++)
            {
                $taxDetail=$resultTaxDetail[$i];
                 if ($taxDetail->getStateAssignedNo() != NULL && $taxDetail->getStateAssignedNo() != "")
                 {
                     $this->assertEqual("060", $taxDetail->getStateAssignedNo());
                     $isStateAssignedNo=TRUE;
                 }
            }
        }
        $this->assertTrue($isStateAssignedNo,"Failed to fetch State Assigned No for the given address");



        $isStateAssignedNo = FALSE;
        $taxLines=$getTaxResult->getTaxLines();
        $taxDetails= $taxLines[0]->getTaxDetails();
        if (count($taxLines) > 0 && count($taxDetails)> 0)
        {

            for($i=0;$i<count($taxDetails);$i++)
            {
                $taxDetail=$taxDetails[$i];
                if ($taxDetail->getStateAssignedNo() != NULL && $taxDetail->getStateAssignedNo() != "")
                {
                    $this->assertEqual("060", $taxDetail->getStateAssignedNo());
                    $isStateAssignedNo = TRUE;
                }
            }
        }
       $this->assertTrue($isStateAssignedNo,"Failed to fetch State Assigned No for the given address");

        // 2. StateAssignedNo is returned by GetTaxHistory
        $historyRequest = new AvaGetTaxHistoryRequest();
        $historyRequest->setCompanyCode($getTaxRequest->getCompanyCode());
        $historyRequest->setDocType($getTaxRequest->getDocType());
        $historyRequest->setDocCode($getTaxRequest->getDocCode());
        $historyRequest->setDetailLevel( AvaDetailLevel::$Diagnostic);

        $historyResult = $client->getTaxHistory($historyRequest);

        $this->assertEqual(AvaSeverityLevel::$Success, $historyResult->getResultCode());
        $this->assertNotNull($historyResult->getGetTaxRequest());
        $this->assertNotNull($historyResult->getGetTaxResult());

        $isStateAssignedNo = FALSE;
        $historyTaxSummary=$historyResult->getGetTaxResult()->getTaxSummary();
        if (count($historyTaxSummary) > 0)
        {
            $taxDetail;
            for($i=0;$i<count($historyTaxSummary);$i++)
            {
                $taxDetail=$historyTaxSummary[$i];
                if ($taxDetail->getStateAssignedNo() != NULL && $taxDetail->getStateAssignedNo()!= "")
                {
                    $this->assertEqual("060", $taxDetail->getStateAssignedNo());
                    $isStateAssignedNo = TRUE;
                }
            }
        }
        $this->assertTrue($isStateAssignedNo,"Failed to fetch State Assigned No for the given address");

        $isStateAssignedNo = FALSE;
        $taxLines=$historyResult->getGetTaxResult()->getTaxLines();
        $taxDetails= $taxLines[0]->getTaxDetails();
        if (count($taxLines) > 0 && count($taxDetails)> 0)
        {

            for($i=0;$i<count($taxDetails);$i++)
            {
                $taxDetail=$taxDetails[$i];
                if ($taxDetail->getStateAssignedNo() != NULL && $taxDetail->getStateAssignedNo() != "")
                {
                    $this->assertEqual("060", $taxDetail->getStateAssignedNo());
                    $isStateAssignedNo = TRUE;
                }
            }
        }
       $this->assertTrue($isStateAssignedNo,"Failed to fetch State Assigned No for the given address");
    }
    	private function CompareHistory($getTaxRequest,$getTaxResult,$getTaxHistoryResult)
		{
		    //global $getTaxResult,$getTaxHistoryRequest,$getTaxRequest,$getTaxHistoryResult;

		    $historyRequest = $getTaxHistoryResult->getGetTaxRequest();
            $historyResult = $getTaxHistoryResult->getGetTaxResult();

	        $this->assertEqual($getTaxRequest->getCompanyCode(), $historyRequest->getCompanyCode());
		    $this->assertEqual($getTaxRequest->getDiscount(), $historyRequest->getDiscount());
	        $this->assertEqual($getTaxRequest->getDocCode(), $historyRequest->getDocCode());
		    $this->assertEqual($getTaxRequest->getDocDate(), $historyRequest->getDocDate());
	        $this->assertEqual($getTaxRequest->getDocType(), $historyRequest->getDocType());
		    $this->assertEqual($getTaxRequest->getExemptionNo(),$historyRequest->getExemptionNo());
	        $this->assertEqual($getTaxRequest->getCustomerCode(),$historyRequest->getCustomerCode());
		    $this->assertEqual($getTaxRequest->getCustomerUsageType(),$historyRequest->getCustomerUsageType());
		    $this->assertEqual($getTaxRequest->getExchangeRate(), $historyRequest->getExchangeRate());
		    $this->assertEqual($getTaxRequest->getExchangeRateEffDate(),$historyRequest->getExchangeRateEffDate());

	        $this->assertEqual($getTaxResult->getDocCode(),$historyResult->getDocCode());
            $this->assertEqual($getTaxResult->getDocDate(), $historyResult->getDocDate());
            $this->assertEqual($getTaxResult->getDocType(), $historyResult->getDocType());
            $this->assertEqual($getTaxResult->getDocStatus(), $historyResult->getDocStatus());
		    $this->assertEqual(new DateTime($getTaxResult->getTimestamp()), new DateTime($historyResult->getTimestamp()));
            $this->assertEqual($getTaxResult->getTotalAmount(), $historyResult->getTotalAmount());
            $this->assertEqual($getTaxResult->getTotalTaxable(), $historyResult->getTotalTaxable());
            $this->assertEqual($getTaxResult->getTotalTax(), $historyResult->getTotalTax());
            $this->assertEqual($getTaxResult->getTotalTaxCalculated(), $historyResult->getTotalTaxCalculated());


            $this->assertEqual(count($getTaxRequest->getLines()),count($historyRequest->getLines()));
            for ($i=0;$i< count($getTaxRequest->getLines());$i++)
            {

				$requestLine =$getTaxRequest->getLines();
				$requestLine=$requestLine[$i];

                $historyLine = $historyRequest->getLine($requestLine->getNo());
                $this->assertNotNull($historyLine);
                $this->assertEqual($requestLine->getNo(), $historyLine->getNo());
                $this->assertEqual($requestLine->getAmount(), $historyLine->getAmount());
                $this->assertEqual($requestLine->getItemCode(),$historyLine->getItemCode());
            }

            //TaxResult.TaxSummary
            for($i=0;$i<count($getTaxResult->getTaxSummary());$i++)
            {
                $resultTaxDetail=$getTaxResult->getTaxSummary();
				$resultTaxDetail=$resultTaxDetail[$i];

				$historyResultTaxDetail=$this->FindTaxDetail($resultTaxDetail->getJurisType(), $resultTaxDetail->getJurisCode(), $historyResult->getTaxSummary());
                $this->assertNotNull($historyResultTaxDetail);

				$this->assertEqual($resultTaxDetail->getCountry(), $historyResultTaxDetail->getCountry());
                $this->assertEqual($resultTaxDetail->getRegion(), $historyResultTaxDetail->getRegion());
                $this->assertEqual($resultTaxDetail->getJurisType(), $historyResultTaxDetail->getJurisType());
                $this->assertEqual($resultTaxDetail->getJurisCode(), $historyResultTaxDetail->getJurisCode());
                $this->assertEqual($resultTaxDetail->getTaxType(), $historyResultTaxDetail->getTaxType());
                $this->assertEqual($resultTaxDetail->getBase(), $historyResultTaxDetail->getBase());
                $this->assertEqual($resultTaxDetail->getTaxable(), $historyResultTaxDetail->getTaxable());
                $this->assertEqual($resultTaxDetail->getRate(),$historyResultTaxDetail->getRate());
                $this->assertEqual($resultTaxDetail->getTax(), $historyResultTaxDetail->getTax());
                $this->assertEqual($resultTaxDetail->getTaxCalculated(), $historyResultTaxDetail->getTaxCalculated());
                $this->assertEqual($resultTaxDetail->getNonTaxable(), $historyResultTaxDetail->getNonTaxable());
                $this->assertEqual($resultTaxDetail->getExemption(), $historyResultTaxDetail->getExemption());
                $this->assertEqual($resultTaxDetail->getJurisName(), $historyResultTaxDetail->getJurisName());
                $this->assertEqual($resultTaxDetail->getTaxName(), $historyResultTaxDetail->getTaxName());
                $this->assertEqual($resultTaxDetail->getTaxAuthorityType(), $historyResultTaxDetail->getTaxAuthorityType());
                $this->assertEqual($resultTaxDetail->getTaxGroup(), $historyResultTaxDetail->getTaxGroup());
            }

            //AvaGetTaxResult.AvaTaxLine
            $this->assertEqual(count($getTaxResult->getTaxLines()), count($historyResult->getTaxLines()));
            for ($i=0;$i<count($getTaxResult->getTaxLines());$i++)
            {

                $resultLine=$getTaxResult->getTaxLines();
				$resultLine=$resultLine[$i];

                $historyResultLine = $historyResult->getTaxLine($resultLine->getNo());

                $this->assertNotNull($historyResultLine);

                $this->assertEqual($resultLine->getNo(), $historyResultLine->getNo());
                $this->assertEqual($resultLine->getTaxable(),$historyResultLine->getTaxable());
                $this->assertEqual($resultLine->getRate(), $historyResultLine->getRate());
                $this->assertEqual($resultLine->getTax(), $historyResultLine->getTax());
                $this->assertEqual($resultLine->getTaxCode(), $historyResultLine->getTaxCode());
                $this->assertEqual($resultLine->getTaxCalculated(),$historyResultLine->getTaxCalculated());
                $this->assertEqual(new DateTime($resultLine->getReportingDate()),new DateTime($historyResultLine->getReportingDate()));
                $this->assertEqual($resultLine->getAccountingMethod(),$historyResultLine->getAccountingMethod());

                // TODO: Addresses


                // Line details
                $this->assertEqual(count($resultLine->getTaxDetails()), count($historyResultLine->getTaxDetails()));
                for ($j=0;$j < count($resultLine->getTaxDetails());$j++)
                {
                    $resultDetail =  $resultLine->getTaxDetails();
		    		$resultDetail=$resultDetail[$j];

                    $historyDetail = $this->FindTaxDetail($resultDetail->getJurisType(), $resultDetail->getJurisCode(), $historyResultLine->getTaxDetails());
                    $this->assertNotNull($historyDetail);

                    $this->assertEqual($resultDetail->getTaxType(), $historyDetail->getTaxType());
                    $this->assertEqual($resultDetail->getBase(), $historyDetail->getBase());
                    $this->assertEqual($resultDetail->getJurisCode(), $historyDetail->getJurisCode());
                    $this->assertEqual($resultDetail->getJurisType(), $historyDetail->getJurisType());
                    $this->assertEqual($resultDetail->getRate(), $historyDetail->getRate());
                    $this->assertEqual($resultDetail->getTax(), $historyDetail->getTax());
                }
            }

		}
		private function CreateTaxRequest($docCode)
	    {


		    $request=new AvaGetTaxRequest();

		    //Set origin Address
		    $origin = new AvaAddress();
		    $origin->setLine1("Avalara");
		    $origin->setLine2("900 winslow way");
		    $origin->setLine3("Suite 100");
		    $origin->setCity("Bainbridge Island");
		    $origin->setRegion("WA");
		    $origin->setPostalCode("98110-1896");
		    $origin->setCountry("USA");
		    $request->setOriginAddress($origin);

		    //Set destination address
		    $destination=  new AvaAddress();
		    $destination->setLine1("3130 Elliott");
		    $destination->setCity("Seattle");
		    $destination->setRegion("WA");
		    $destination->setPostalCode("98121");
		    $destination->setCountry("USA");
		    $request->setDestinationAddress($destination);

		    //Set line
		    $line1 = new AvaLine();
		    $line1->setNo ("1");                  //string  // line Number of invoice
		    $line1->setItemCode("SKU123");            //string
		    $line1->setDescription("Invoice Calculated From PHP SDK");         //string
		    $line1->setTaxCode("");             //string
		    $line1->setQty(1.0);                 //decimal
		    $line1->setAmount(1000.00);              //decimal // TotalAmmount
		    $line1->setDiscounted(FALSE);          //boolean
		    $line1->setRevAcct("");             //string
		    $line1->setRef1("");                //string
		    $line1->setRef2("");                //string
		    $line1->setExemptionNo("");         //string
		    $line1->setCustomerUsageType("");   //string


	        $line2 = new AvaLine();
		    $line2->setNo ("2");                  //string  // line Number of invoice
		    $line2->setItemCode("SKU124");            //string
		    $line2->setDescription("Invoice Calculated From PHP SDK");         //string
		    $line2->setTaxCode("");             //string
		    $line2->setQty(1.0);                 //decimal
		    $line2->setAmount(10.00);              //decimal // TotalAmmount
		    $line2->setDiscounted(FALSE);          //boolean
		    $line2->setRevAcct("");             //string
		    $line2->setRef1("");                //string
		    $line2->setRef2("");                //string
		    $line2->setExemptionNo("");         //string
		    $line2->setCustomerUsageType("");   //string

	        $request->setLines(array ($line1,$line2));

		    $request->setCompanyCode('DEFAULT');         // Your Company Code From the Dashboard
		    $request->setDocType(AvaDocumentType::$SalesInvoice);   	// Only supported types are SalesInvoice or SalesOrder

		    //$dateTime=new DateTime();
		    //$docCode= "PHP".date_format($dateTime,"dmyGis");
		    $request->setDocCode($docCode);             //    invoice number

		    $dateTime=new DateTime();
		    $docDate=date_format($dateTime,"Y-m-d");
		    //$request->setDocDate("2008-01-24");           //date
		    $request->setDocDate($docDate);           //date
		    $request->setSalespersonCode("");             // string Optional
		    $request->setCustomerCode("Cust123");        //string Required
		    $request->setCustomerUsageType("");   //string   Entity Usage
		    $request->setDiscount(0.00);            //decimal
		    $request->setPurchaseOrderNo("");     //string Optional
		    $request->setExemptionNo("");         //string   if not using ECMS which keys on customer code

		    $request->setDetailLevel(AvaDetailLevel::$Diagnostic);         //Summary or Document or Line or Tax or Diagnostic

		    $request->setReferenceCode("Reference");       //string Optional
		    $request->setLocationCode("");        //string Optional - aka outlet id for tax forms



		    return $request;
		}
		private function CreateTaxRequestForTaxOverride($docCode)
		{
		    $request = new AvaGetTaxRequest();

		    $request->setCompanyCode("DEFAULT");
		    $request->setDocCode($docCode);
		    $request->setDocType(AvaDocumentType::$SalesInvoice);

		    $dateTime=new DateTime();
		    $docDate=date_format($dateTime,"Y-m-d");
		    $request->setDocDate($docDate);
		    $request->setCustomerCode("TaxSvcTest");
		    $request->setSalespersonCode("");
		    $request->setDetailLevel(AvaDetailLevel::$Tax);

		    $origin = new AvaAddress();
		    $origin->setAddressCode("Origin");
		    $origin->setCity("Denver");
		    $origin->setRegion("CO");
		    $origin->setPostalCode("80216-1022");
		    $origin->setCountry("USA");
		    $request->setOriginAddress($origin);

		    $destination = new AvaAddress();
		    $destination->setAddressCode("Dest");
		    $destination->setLine1("11051 S Parker Rd");
		    $destination->setCity("Parker");
		    $destination->setRegion("CO");
		    $destination->setPostalCode("80134-7441");
		    $destination->setCountry("USA");
		    $request->setDestinationAddress($destination);

		    $line = new AvaLine();
		    $line->setNo("1");
		    $line->setQty(1);
		    $line->setAmount(1000);

		    $taxOverride = new AvaTaxOverride();
		    $taxOverride->setTaxOverrideType(AvaTaxOverrideType::$TaxAmount);
		    $taxOverride->setTaxAmount(5);
		    $dateTime=new DateTime();
		    $taxOverride->setTaxDate(date_format($dateTime,"Y-m-d"));
		    $taxOverride->setReason("Return");
		    $line->setTaxOverride($taxOverride);

		    $request->setLines(array ($line));

		    return $request;
		}
		// Function for Test Tax Override Type
        private function CreateTaxRequestForTaxOverrideType($docCode){
            $request = new AvaGetTaxRequest();

            $request->setCompanyCode("DEFAULT");
            $request->setDocCode($docCode);
            $request->setDocType(AvaDocumentType::$PurchaseOrder);

            $dateTime=new DateTime();
            $docDate=date_format($dateTime,"Y-m-d");
            $request->setDocDate($docDate);
            $request->setCustomerCode("TaxSvcTest");
            $request->setSalespersonCode("");
            $request->setDetailLevel(AvaDetailLevel::$Tax);

            $origin = new AvaAddress();
            $origin->setAddressCode("Origin");
            $origin->setCity("Denver");
            $origin->setRegion("CO");
            $origin->setPostalCode("80216-1022");
            $origin->setCountry("USA");
            $request->setOriginAddress($origin);

            $destination = new AvaAddress();
            $destination->setAddressCode("Dest");
            $destination->setLine1("11051 S Parker Rd");
            $destination->setCity("Parker");
            $destination->setRegion("CO");
            $destination->setPostalCode("80134-7441");
            $destination->setCountry("USA");
            $request->setDestinationAddress($destination);

            $line = new AvaLine();
            $line->setNo("1");
            $line->setQty(1);
            $line->setAmount(0);

            $taxOverride = new AvaTaxOverride();
            $taxOverride->setTaxOverrideType(AvaTaxOverrideType::$AccruedTaxAmount);
            $taxOverride->setTaxAmount(21.1);
            $taxOverride->setReason("Accrued");
            $line->setTaxOverride($taxOverride);

            $request->setLines(array ($line));

            return $request;
        }
        //Function for Document Type
        private function CreateTaxRequestDocType($docType)
        {
            $request=new AvaGetTaxRequest();

            //Set origin Address
            $origin = new AvaAddress();
            $origin->setLine1("Avalara");
            $origin->setLine2("900 winslow way");
            $origin->setLine3("Suite 100");
            $origin->setCity("Bainbridge Island");
            $origin->setRegion("WA");
            $origin->setPostalCode("98110-1896");
            $origin->setCountry("USA");
            $request->setOriginAddress($origin);

            //Set destination address
            $destination=  new AvaAddress();
            $destination->setLine1("3130 Elliott");
            $destination->setCity("Seattle");
            $destination->setRegion("WA");
            $destination->setPostalCode("98121");
            $destination->setCountry("USA");
            $request->setDestinationAddress($destination);

            //Set line
            $line1 = new AvaLine();
            $line1->setNo ("1");                  //string  // line Number of invoice
            $line1->setItemCode("INV123");            //string
            $line1->setQty(1.0);                 //decimal
            $line1->setAmount(1000.00);              //decimal // TotalAmmount

            $line2 = new AvaLine();
            $line2->setNo ("2");                  //string  // line Number of invoice
            $line2->setItemCode("INV124");            //string
            $line2->setQty(1.0);                 //decimal
            $line2->setAmount(10.00);              //decimal // TotalAmmount

            $request->setLines(array ($line1,$line2));

            $request->setCompanyCode('DEFAULT');         // Your Company Code From the Dashboard
            $request->setDocCode("DocTypeTest");
            $request->setDocType($docType);
            $request->setDocDate(date_format(new DateTime(),"Y-m-d"));
            $request->setCustomerCode("TaxSvcTest");        //string Required
            $request->setSalespersonCode("");             // string Optional
            $request->setDetailLevel(AvaDetailLevel::$Diagnostic);         //Summary or Document or Line or Tax or Diagnostic


        return $request;
    }

    private function CreateTaxRequestForBINo($bino)
    {
        $request=new AvaGetTaxRequest();

        //Set origin Address
        $origin = new AvaAddress();
        $origin->setLine1("Avalara");
        $origin->setLine2("900 winslow way");
        $origin->setLine3("Suite 100");
        $origin->setCity("Bainbridge Island");
        $origin->setRegion("WA");
        $origin->setPostalCode("98110-1896");
        $origin->setCountry("USA");
        $request->setOriginAddress($origin);

        //Set destination address
        $destination=  new AvaAddress();
        $destination->setLine1("3130 Elliott");
        $destination->setCity("Seattle");
        $destination->setRegion("WA");
        $destination->setPostalCode("98121");
        $destination->setCountry("USA");
        $request->setDestinationAddress($destination);

        //Set line
        $line = new AvaLine();
        $line->setNo ("1");                  //string  // line Number of invoice
        $line->setBusinessIdentificationNo("LL123");
        $line->setItemCode("Item123");            //string
        $line->setQty(1.0);                 //decimal
        $line->setAmount(1010.00);

        $request->setLines(array ($line));

        $request->setCompanyCode('DEFAULT');         // Your Company Code From the Dashboard
        $request->setDocCode("DocTypeTest");
        $request->setBusinessIdentificationNo($bino);
        $request->setDocDate(date_format(new DateTime(),"Y-m-d"));
        $request->setCustomerCode("TaxSvcTest");        //string Required
        $request->setSalespersonCode("");             // string Optional
        $request->setDetailLevel(AvaDetailLevel::$Tax);         //Summary or Document or Line or Tax or Diagnostic

            return $request;
        }
	    private function waitForTaxHistory($getTaxHistoryRequest)
	    {
	        global $client;
	    	$result = NULL;

	        $retryCount = 0;
	        do
	        {
	            $result = $client->getTaxHistory($getTaxHistoryRequest);
	            $message;
	            if($result->getResultCode() == AvaSeverityLevel::$Error)
	            {
		            $messages= $result->getMessages();
		            $message=$messages[0];
	            }
	            if ($result->getResultCode() != AvaSeverityLevel::$Error ||
	                ($result->getMessages() != NULL && $message->getName()=="DocumentNotFoundError"))
	            {
	                break;
	            }
	            try
	            {
	                sleep(1); //time in seconds
	            }
	            catch (Exception $ex)
	            {
	                // Ignore
	            }
	            $retryCount=$retryCount+1;
	        } while (retryCount <= 10);

	        return $result;
	    }
		private function FindTaxDetail($jurisdictionType, $jurisdictionCode, $taxDetails)
        {
            $match = NULL;

            for ($i=0;$i<count($taxDetails);$i++)
            {
                $detail = $taxDetails[$i];
                if ( ($detail->getJurisType()==$jurisdictionType) && ($detail->getJurisCode()==$jurisdictionCode))
                {
                    $match = $detail;
                    break;
                }
            }

            return $match;
        }

    }


?>
